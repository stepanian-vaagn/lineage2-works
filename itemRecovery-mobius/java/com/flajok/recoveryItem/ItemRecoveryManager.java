package com.flajok.recoveryItem;

import org.l2jmobius.commons.database.DatabaseFactory;
import org.l2jmobius.gameserver.idfactory.IdFactory;
import org.l2jmobius.gameserver.model.actor.instance.PlayerInstance;
import org.l2jmobius.gameserver.model.items.instance.ItemInstance;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class ItemRecoveryManager {

    private static final Logger LOGGER = Logger.getLogger(ItemRecoveryManager.class.getName());


    public ItemRecoveryManager() {
        load();
    }


    public void load() {

        List<ItemRecovery> itemList = new ArrayList<>();

        ItemRecovery item;

        try (Connection con = DatabaseFactory.getConnection();
             PreparedStatement statement = con.prepareStatement("SELECT `object_id`, `time` FROM item_recovery")) {

            try (ResultSet rset = statement.executeQuery()) {

                while (rset.next()) {

                    item = new ItemRecovery();
                    item.setObject_id(rset.getInt("object_id"));
                    item.setTime(rset.getLong("time"));

                    itemList.add(item);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (itemList.isEmpty()) {
            LOGGER.info(getClass().getSimpleName() + ": Delete item's " + itemList.size() + ".");
            return;
        }


        int counter = 0;
        for (ItemRecovery elem : itemList) {
            if (checkTime(elem)) {
                counter++;
            }

        }

        LOGGER.info(getClass().getSimpleName() + ": Delete item's " + counter + ".");

    }

    public boolean checkTime(ItemRecovery itemRecovery) {

        if (Timestamp.valueOf(LocalDateTime.now()).after(new Timestamp(itemRecovery.getTime()))) {
            deleteItem(itemRecovery);
            return true;
        } else {
            return false;
        }
    }

    private void deleteItem(ItemRecovery itemRecovery) {

        try (Connection con = DatabaseFactory.getConnection();
             PreparedStatement statement = con.prepareStatement("DELETE FROM item_recovery WHERE object_id=?")) {

            statement.setInt(1, itemRecovery.getObject_id());
            statement.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void recoveryItem(int objectId, PlayerInstance player) {

        final ItemRecovery item = new ItemRecovery();

        try (Connection con = DatabaseFactory.getConnection();
             PreparedStatement statement = con.prepareStatement("SELECT * FROM item_recovery WHERE charId=? AND object_id=?")) {
            statement.setInt(1, player.getObjectId());
            statement.setInt(2, objectId);

            try (ResultSet rset = statement.executeQuery()) {

                while (rset.next()) {
                    item.setItem_id(rset.getInt("item_id"));
                    item.setObject_id(rset.getInt("object_id"));
                    item.setCount(rset.getLong("count"));
                    item.setEnchant_level(rset.getInt("enchant_level"));
                    item.setTime(rset.getLong("time"));
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (ItemRecoveryManager.getInstance().checkTime(item)) {
            return;
        }

        final ItemInstance itemRecovery = new ItemInstance(IdFactory.getInstance().getNextId(), item.getItem_id());

        if (item.getEnchant_level() != 0) {
            itemRecovery.setEnchantLevel(item.getEnchant_level());
        }

        itemRecovery.setCount(item.getCount());

        player.addItem("Recovery Item", itemRecovery, player, true);

        deleteItem(item);
    }



    public static ItemRecoveryManager getInstance()
    {
        return ItemRecoveryManager.SingletonHolder.INSTANCE;
    }

    private static class SingletonHolder
    {
        protected static final ItemRecoveryManager INSTANCE = new ItemRecoveryManager();
    }
}

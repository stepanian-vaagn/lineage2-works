package com.flajok.recoveryItem;

public class ItemRecovery {

    private int charId;
    private int item_id;
    private int object_id;
    private long count;
    private int enchant_level;
    private long time;

    public ItemRecovery() {
    }


    public int getCharId() {
        return charId;
    }

    public void setCharId(int charId) {
        this.charId = charId;
    }

    public int getItem_id() {
        return item_id;
    }

    public void setItem_id(int item_id) {
        this.item_id = item_id;
    }

    public int getObject_id() {
        return object_id;
    }

    public void setObject_id(int object_id) {
        this.object_id = object_id;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public int getEnchant_level() {
        return enchant_level;
    }

    public void setEnchant_level(int enchant_level) {
        this.enchant_level = enchant_level;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }
}

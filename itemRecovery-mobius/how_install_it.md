**Hello, i will explain how install it to your project.**

**Step 1.**<br>
Open GameServer.java
Add this code<br>
`ItemRecoveryManager.getInstance();`

**Step 2.**<br>
Open Config.java

Create new variable<br>
`	
private static final String CUSTOM_ITEM_RECOVERY_FILE = "./config/custom/ItemRecovery.ini";
`
<br>

on method load() put inside this <br>
`
// Load ItemRecovery custom config file
final  PropertiesParser itemRecovery = new PropertiesParser(CUSTOM_ITEM_RECOVERY_FILE);
TIME_SAVE_ITEM = itemRecovery.getInt("ItemTimeSave", 1);
`
<br>

**Step 3.**<br>
 RequestDestroyItem.java Go to line 226-227
find :
 			{
 				iu.addModifiedItem(removedItem);
 			}
 			player.sendInventoryUpdate(iu);
 		}
make :
 			{
 				iu.addModifiedItem(removedItem);
 			}
			saveToRecoveryItem(player, removedItem, count);
 			player.sendInventoryUpdate(iu);
 		}
<br>
in the end file of RequestDestroyItem.java add this method<br>

	private void saveToRecoveryItem(PlayerInstance playerInstance ,ItemInstance itemInstance, long count) {
		try (Connection con = DatabaseFactory.getConnection();
			 PreparedStatement ps = con.prepareStatement("INSERT INTO item_recovery (`charId`, `item_id`, `object_id`, `count`, `enchant_level`, `time`) VALUES (?, ?, ?, ?, ?, ?) "))
		{
			ps.setInt(1, playerInstance.getObjectId());
			ps.setInt(2, itemInstance.getId());
			ps.setInt(3, IdFactory.getInstance().getNextId());
			ps.setLong(4, count);
			ps.setInt(5, itemInstance.getEnchantLevel());
			ps.setLong(6, Timestamp.valueOf(LocalDateTime.now().plusDays(Config.TIME_SAVE_ITEM)).getTime());
			ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
**Step 4.**<br>
Add ItemRecoveryBoard to MasterHandler.java this ItemRecoveryBoard.class

`
			// Community Board
			ClanBoard.class,
			FavoriteBoard.class,
			FriendsBoard.class,
			HomeBoard.class,
			HomepageBoard.class,
			MailBoard.class,
			MemoBoard.class,
			RegionBoard.class,
			DropSearchBoard.class,
			ItemRecoveryBoard.class
`
<br>

**Step 5.**<br>
add to navigation.html /data/html/communityboard/custom/navigation.html

button with bypass "bypass _bbs_item_show 0"

<br>

**Step 6.**<br>
Add includes files from this project
SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for item_recovery
-- ----------------------------
DROP TABLE IF EXISTS `item_recovery`;
CREATE TABLE `item_recovery`  (
  `charId` int(10) NOT NULL,
  `item_id` int(11) NOT NULL,
  `object_id` int(11) NOT NULL,
  `count` int(20) NULL DEFAULT 0,
  `enchant_level` int(11) NULL DEFAULT 0,
  `time` bigint(13) NULL DEFAULT NULL,
  PRIMARY KEY (`object_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;

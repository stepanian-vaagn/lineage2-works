package handlers.communityboard;

import com.flajok.recoveryItem.ItemRecovery;
import com.flajok.recoveryItem.ItemRecoveryManager;
import org.l2jmobius.commons.database.DatabaseFactory;
import org.l2jmobius.gameserver.cache.HtmCache;
import org.l2jmobius.gameserver.datatables.ItemTable;
import org.l2jmobius.gameserver.handler.CommunityBoardHandler;
import org.l2jmobius.gameserver.handler.IParseBoardHandler;
import org.l2jmobius.gameserver.model.actor.instance.PlayerInstance;
import org.l2jmobius.gameserver.model.html.PageBuilder;
import org.l2jmobius.gameserver.model.html.PageResult;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class ItemRecoveryBoard implements IParseBoardHandler {

    private static final String NAVIGATION_PATH = "data/html/CommunityBoard/Custom/navigation.html";
    private static final String[] COMMAND =
            {
                    "_bbs_item_show",
                    "_bbs_item_recovery"
            };

    @Override
    public boolean parseCommunityBoardCommand(String command, PlayerInstance player) {

        final String navigation = HtmCache.getInstance().getHtm(player, NAVIGATION_PATH);

        String returnHtml = HtmCache.getInstance().getHtm(player, "data/html/CommunityBoard/Custom/itemRecovery/main.html");

        StringTokenizer st = new StringTokenizer(command, " ");
        st.nextToken();

        if (command.startsWith("_bbs_item_show")) {
            if (!st.hasMoreTokens()) {
                return false;
            }

            returnHtml = itemRecovery(player, returnHtml, Integer.parseInt(st.nextToken()));

        } else if (command.startsWith("_bbs_item_recovery")) {
            if (!st.hasMoreTokens()) {
                return false;
            }

            ItemRecoveryManager.getInstance().recoveryItem(Integer.parseInt(st.nextToken()), player);

            returnHtml = itemRecovery(player, returnHtml, 0);

        }

        if (returnHtml != null) {

            returnHtml = returnHtml.replace("%navigation%", navigation);

            CommunityBoardHandler.separateAndSend(returnHtml, player);
        }

        return false;
    }

    private String itemRecovery(PlayerInstance player, String html, int page) {

        List<ItemRecovery> itemList = new ArrayList<>();

        ItemRecovery item;

        try (Connection con = DatabaseFactory.getConnection();
             PreparedStatement statement = con.prepareStatement("SELECT * FROM item_recovery WHERE charId=?")) {
            statement.setInt(1, player.getObjectId());

            try (ResultSet rset = statement.executeQuery()) {

                while (rset.next()) {

                    item = new ItemRecovery();
                    item.setItem_id(rset.getInt("item_id"));
                    item.setObject_id(rset.getInt("object_id"));
                    item.setCount(rset.getLong("count"));
                    item.setEnchant_level(rset.getInt("enchant_level"));
                    item.setTime(rset.getLong("time"));

                    if (!ItemRecoveryManager.getInstance().checkTime(item)) {
                        itemList.add(item);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        SimpleDateFormat sdf =
                new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        final PageResult result = PageBuilder.newBuilder(itemList, 13, "bypass _bbs_item_show").currentPage(page).bodyHandler((pages, elem, sb) ->
        {
            sb.append("<tr>");
            sb.append("<td width=\"32\"><img src=\"" + ItemTable.getInstance().getTemplate(elem.getItem_id()).getIcon() + "\" width=32 height=32></td>");
            sb.append("<td width=\"155\">" + ItemTable.getInstance().getTemplate(elem.getItem_id()).getName() + "</td>");
            sb.append("<td width=\"50\">" + elem.getCount() + "</td>");
            sb.append("<td width=\"60\">+" + elem.getEnchant_level() + "</td>");
            sb.append("<td width=\"155\">" + sdf.format(elem.getTime()) + "</td>");
            sb.append("<td width=\"155\"><button value=\"Recovery\" action=\"bypass _bbs_item_recovery " + elem.getObject_id() + "\" width=85 height=27 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"/></td>");
            sb.append("</tr>");
        }).build();

        StringBuilder _sb = new StringBuilder();

        _sb.append("<table width=555 valign=\"bottom\"><tr>");

        int pagePrev = page == 0 ? 0 : page - 1;
        _sb.append("<td width=100 align=left><button value=\"Prev\" action=\"bypass _bbs_item_show " + pagePrev + "\" width=120 height=26 back=\"L2UI_CT1.Button_DF_Large_Down\" fore=\"L2UI_CT1.Button_DF_Large\"></td>");
        _sb.append("<td width=300><center>Page <font color=\"LEVEL\">" + (page + 1) + "</font> of <font color=\"LEVEL\">" + result.getPages() + "</font></center></td>");

        int pageNext = page == (result.getPages() - 1) ? page : page + 1;
        _sb.append("<td width=100 align=right><button value=\"Next\" action=\"bypass _bbs_item_show " + pageNext + "\" width=120 height=26 back=\"L2UI_CT1.Button_DF_Large_Down\" fore=\"L2UI_CT1.Button_DF_Large\"></td>");
        _sb.append("</tr></table>");


        html = html.replace("%pages%", _sb.toString());
        html = html.replace("%content%", result.getBodyTemplate().toString());

        return html;

    }


    @Override
    public String[] getCommunityBoardCommands() {
        return COMMAND;
    }
}
